//$Id: Position.cpp,v 1.1.1.1 2012/04/08 04:00:33 kyle Exp $
#include "Position.h"

Position::Position()
{
  xpos = 0;
  ypos = 0;
  zpos = 0;
  xrot = 0;
  yrot = 0;
  zrot = 0;
}

void Position::SetPos(float x, float y, float z)
{
  xpos = x;
  ypos = y;
  zpos = z;
}

void Position::SetRot(float x, float y, float z)
{
  xrot = x;
  yrot = y;
  zrot = z;
}

float Position::pX(){return xpos;}
float Position::pY(){return ypos;}
float Position::pZ(){return zpos;}
float Position::rX(){return xrot;}
float Position::rY(){return yrot;}
float Position::rZ(){return zrot;}

void Position::MovX(float x){xpos += x;}
void Position::MovY(float y){ypos += y;}
void Position::MovZ(float z){zpos += z;}
void Position::RotX(float x){xrot += x;}
void Position::RotY(float y){yrot += y;}
void Position::RotZ(float z){zrot += z;}
