//$Id: GameLoop.cpp,v 1.6 2012/04/09 00:10:42 kyle Exp $
#include "GameLoop.h"

GameLoop::GameLoop(GameState *theGame)
{
    //you just lost
    game = theGame;
    lastTime = 0;
    rotationMultiplier = .25;
}
void GameLoop::GetInput()
{
  game->ryvelocity = 0;
  game->rxvelocity = 0;
  SDL_Event event;
  while(SDL_PollEvent(&event))
    {
      if(event.type == SDL_MOUSEMOTION && game->TrapMouse())
        {
          int y;
          int halfWidth = game->width>>1;
          int halfHeight = game->height>>1;
          SDL_GetMouseState(NULL, &y);

          //          printf("Mouse Y: %d\nMoved by %d, %d to (%d, %d)\n", y,
          //       event.motion.xrel, event.motion.yrel,
          //       event.motion.x, event.motion.y);

          if(y <= 0)
            y=game->height;

          game->ryvelocity = -event.motion.xrel/2;
          //game->ryvelocity = -event.motion.xrel/2;
          game->rxvelocity = -event.motion.yrel/2;

          SDL_WarpMouse(halfWidth,halfHeight);

          /*
          if(y > (halfHeight)+90)
            {
              SDL_WarpMouse(halfWidth, (halfHeight)+90);
              game->camPosition.SetRot(90,game->camPosition.rY(),0);
            }
          else if(y < (halfHeight)-90)
            {
              SDL_WarpMouse(halfWidth, (halfHeight)-90);
              game->camPosition.SetRot(-90,game->camPosition.rY(),0);
            }
          else
            {
              SDL_WarpMouse(halfWidth,y);
              game->camPosition.SetRot((y-(halfHeight))*rotationMultiplier,game->camPosition.rY(),0);
            }
          */
        }
      else if(event.type == SDL_KEYDOWN)
        {
          SDLKey key = event.key.keysym.sym;
          if(key == SDLK_ESCAPE)
            game->Kill();
          else if(key == SDLK_w)
            game->zvelocity = -0.3f;
          else if(key == SDLK_s)
            game->zvelocity = 0.3f;
          else if(key == SDLK_a)
            game->xvelocity = -0.3f;
          else if(key == SDLK_d)
            game->xvelocity = 0.3f;
          else if(key == SDLK_q)
            game->yvelocity = -1.1f;
          else if(key == SDLK_e)
            game->yvelocity = 1.1f;
          else if(key == SDLK_m)
            {
              if(game->TrapMouse())
                SDL_ShowCursor(SDL_ENABLE);
              else
                SDL_ShowCursor(SDL_DISABLE);
              game->TrapMouse(!game->TrapMouse());
            }
        }
      else if(event.type == SDL_KEYUP)
        {
          SDLKey key = event.key.keysym.sym;
          if(key == SDLK_ESCAPE)
            game->Kill();
          else if(key == SDLK_w)
            game->zvelocity = 0.0f;
          else if(key == SDLK_s)
            game->zvelocity = 0.0f;
          else if(key == SDLK_a)
            game->xvelocity = 0.0f;
          else if(key == SDLK_d)
            game->xvelocity = 0.0f;
          else if(key == SDLK_q)
            game->yvelocity = 0.0f;
          else if(key == SDLK_e)
            game->yvelocity = 0.0f;
        }
      else if(event.type == SDL_VIDEORESIZE)
        {
          /*SDL_ResizeEvent resizeEvent = event.resize;
            game->width = resizeEvent.w;
            game->height = resizeEvent.h;
            game->InitSDL();
            game->InitGLContext();
          */
        }
    }
}

void GameLoop::Update()
{
  //Camera movement
  float toRad = PI/180;
  
  game->camPosition.MovX(game->zvelocity*-sin(game->camPosition.rY()*toRad));
  game->camPosition.MovZ(game->zvelocity*cos(game->camPosition.rY()*toRad));
  game->camPosition.MovX(game->xvelocity*cos(game->camPosition.rY()*toRad));
  game->camPosition.MovZ(game->xvelocity*sin(game->camPosition.rY()*toRad));
  game->camPosition.MovY(game->yvelocity);
  game->camPosition.RotY(game->ryvelocity * rotationMultiplier);
  game->camPosition.RotX(game->rxvelocity * rotationMultiplier);

  //  printf("ryvelocity: %f, rxvelocity %f\n", game->ryvelocity, game->rxvelocity);
  
  /*
    game->camPosition.MovX(
    game->xvelocity*-sin(game->camPosition.rY()*toRad)
    +game->xvelocity*cos(game->camPosition.rY()*toRad));
    game->camPosition.MovZ(
    game->zvelocity*cos(game->camPosition.rY()*toRad)
    +game->xvelocity*sin(game->camPosition.rY()*toRad));
    game->camPosition.MovY(game->yvelocity);
    game->camPosition.RotY(game->ryvelocity);
  */
  int TREE = 1;
  int FLOWER = 2;
  
  float pX = game->camPosition.pX();
  float pZ = game->camPosition.pZ();
  
  //Tree and Flower generation
  
  for(int i = 0; i < game->squares.size(); i++)
    {
      float x = pX - game->squares[i].position.pX();
      float z = pZ - game->squares[i].position.pZ();
      float d = sqrt(pow(x,2)+pow(z,2));
      if(d >= 130.0f)
        {
          //float pX = game->camPosition.pX();
          //float pZ = game->camPosition.pZ();
          //if(s.GetType() == FLOWER)
          // {
          //   game->squares->erase(iterator++);
          //   Trees::addFlower(game->squares, pX-120,pX+120,pZ-120,pZ+120);
          // }
          //else if(s.GetType() == TREE)
          // { 
          float newx = game->squares[i].position.pX() + (2*(pX - game->squares[i].position.pX()));
          float newz = game->squares[i].position.pZ() + (2*(pZ - game->squares[i].position.pZ()));
          game->squares[i].position.SetPos(newx, game->squares[i].position.pY(), newz);
          //}
        }
      /*
        cout<<"Tree X: "<<s.position.pX()<<" | ";
        cout<<"Tree Z: "<<s.position.pZ()<<" | ";      
        cout<<"Cam X: "<<game->camPosition.pX()<<" | ";
        cout<<"Cam Y: "<<game->camPosition.pY()<<" | ";
        cout<<"Cam Z: "<<game->camPosition.pZ()<<" | ";
        cout<<"Distance: "<<d<<endl;
        cout.flush();
        /**/
    }
  
  /*BLOCK
    list<Square>::iterator iterator;
    for(iterator = game->squares.begin(); iterator != game->squares.end(); ++iterator)
    {
    Square s = *iterator;
    float x = pX - s.position.pX();
    float z = pZ - s.position.pZ();
    float d = sqrt(pow(x,2)+pow(z,2));
    if(d >= 130.0f)
	{
    //float pX = game->camPosition.pX();
    //float pZ = game->camPosition.pZ();
    //if(s.GetType() == FLOWER)
    // {
    //   game->squares->erase(iterator++);
    //   Trees::addFlower(game->squares, pX-120,pX+120,pZ-120,pZ+120);
    // }
    //else if(s.GetType() == TREE)
    // { 
    float newx = s.position.pX() + (2*(pX - s.position.pX()));
    float newz = s.position.pZ() + (2*(pZ - s.position.pZ()));
    s.position.SetPos(newx, s.position.pY(), newz);
    *iterator = s;
    //}
	}
    /*
    cout<<"Tree X: "<<s.position.pX()<<" | ";
    cout<<"Tree Z: "<<s.position.pZ()<<" | ";      
    cout<<"Cam X: "<<game->camPosition.pX()<<" | ";
    cout<<"Cam Y: "<<game->camPosition.pY()<<" | ";
    cout<<"Cam Z: "<<game->camPosition.pZ()<<" | ";
    cout<<"Distance: "<<d<<endl;
    cout.flush();
  */
  
  /*BLOCK
    }  
  */
  /*
    Uint32 timeBetween = SDL_GetTicks() - lastTime;
    float FPS = 0;
    FPS = 1000/(SDL_GetTicks() - lastTime);
    cout<<"FPS: "<<FPS<<" | ";
    cout<<"SDL Ticks: "<<SDL_GetTicks()<<" | ";
    cout<<"Time Between: "<<timeBetween<<endl;
    lastTime = SDL_GetTicks();
    /**/
}

void GameLoop::Render()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glLoadIdentity();

  glRotatef(game->camPosition.rX(), 1.0f,0.0f,0.0f);
  glRotatef(game->camPosition.rY(), 0.0f,1.0f,0.0f);
  glRotatef(game->camPosition.rZ(), 0.0f,0.0f,1.0f);
  
  //glDisable(GL_LIGHTING);
  glDisable(GL_FOG);
  glPushMatrix();
  game->floor.Render(game->camPosition);
  glPopMatrix();
  //glEnable(GL_LIGHTING);
  glEnable(GL_FOG);
  
  
  glTranslatef(-game->camPosition.pX(), -game->camPosition.pY(), -game->camPosition.pZ());

  GLfloat LightPosition[] = {-100.0f,1.0f,-100.0f,1.0f};
  glLightfv(GL_LIGHT1, GL_POSITION, LightPosition);

  for(int i = 0; i < game->squares.size(); i++)
    {
      glPushMatrix();
      game->squares[i].Render(game->camPosition);
      glPopMatrix();
    }

  /*
    list<Square>::const_iterator iterator;
    for(iterator = game->squares.begin(); iterator != game->squares.end(); ++iterator)
    {
    glPushMatrix();
    Square s = *iterator;
    s.Render(game->camPosition);
    glPopMatrix();
    }

  */
  SDL_GL_SwapBuffers();
}
