//$Id: Square.cpp,v 1.3 2012/04/08 23:10:52 kyle Exp $
#include "Square.h"

void Square::Render(Position &camPosition)
{
  glTranslatef(position.pX(),position.pY(),position.pZ());
  if(followCam)
    {
      glRotatef(-camPosition.rY(), 0.0f,1.0f,0.0f);
    }
  else
    {
      glRotatef(position.rX(),1.0f,0.0f,0.0f);
      glRotatef(position.rY(),0.0f,1.0f,0.0f);
      glRotatef(position.rZ(),0.0f,0.0f,1.0f);
    }
  glBindTexture(GL_TEXTURE_2D, texture);

  float homex = 0.0f;
  float homey = 0.0f;
  if(fakeMotion)
    {
      homex = (camPosition.pX() - (int)camPosition.pX());
      homey = -(camPosition.pZ() - (int)camPosition.pZ());
 
      glBegin(GL_QUADS);
      {
	glNormal3f(0.0f,1.0f,0.0f);
	glTexCoord2f(homex,texRepeaty + homey); glVertex3f(-width/2,-height/2,0.0f);                  //bottom left
	
	glNormal3f(0.0f,1.0f,0.0f);
	glTexCoord2f(homex,homey);  glVertex3f(-width/2,height/2,0.0f);            //upper left
	
	glNormal3f(0.0f,1.0f,0.0f);
	glTexCoord2f(texRepeatx + homex,homey);   glVertex3f(width/2,height/2,0.0f);      //upper right

	glNormal3f(0.0f,1.0f,0.0f);
	glTexCoord2f(texRepeatx + homex,texRepeaty + homey);  glVertex3f(width/2,-height/2,0.0f);            //bottom right
      }
      glEnd();
    }
  else
    {
      glCallList(displayList);
      /*
      glBegin(GL_QUADS);
      {
	glTexCoord2f(0.0f,texRepeaty);       glVertex3f(-width/2,-height/2,0.0f);
	glTexCoord2f(0.0f,0.0f);             glVertex3f(-width/2,height/2,0.0f);
	glTexCoord2f(texRepeatx,0.0f);       glVertex3f(width/2,height/2,0.0f);
	glTexCoord2f(texRepeatx,texRepeaty); glVertex3f(width/2,-height/2,0.0f);
      }
      glEnd();
      /**/
    }
}

GLuint* Square::Texture()
{
  return &texture;
}

Square::Square()
{
  width = 0;
  height = 0;
  texRepeatx = 1.0f;
  texRepeaty = 1.0f;
  followCam = false;
  fakeMotion = false;
}

void Square::SetDisplayList(GLuint list)
{
  displayList = list;
}

void Square::SetTex(GLuint tex)
{
  texture = tex;
}

void Square::SetTex(char * texName)
{
  FileFuncs::LoadGLTexture(texture, texName, GL_NEAREST, GL_NEAREST);
}

void Square::SetTexRepeat(float repeatX, float repeatY)
{
  texRepeatx = repeatX;
  texRepeaty = repeatY;
}

void Square::SetSize(float w, float h)
{
  width = w;
  height = h;
}
void Square::SetType(int inType){type = inType;}
int Square::GetType(){return type;}
void Square::FollowCam(bool val){followCam = val;}
void Square::FakeMotion(bool val){fakeMotion = val;}
float Square::TexRepeatX(){return texRepeatx;}
float Square::TexRepeatY(){return texRepeaty;}
float Square::Width(){return width;}
float Square::Height(){return height;}
