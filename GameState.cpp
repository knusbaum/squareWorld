//$Id: GameState.cpp,v 1.5.2.1 2012/04/09 00:17:24 kyle Exp $
#include "GameState.h"

using namespace std;

bool GameState::Running(){return running;}
void GameState::Kill(){ running = false;}
void GameState::Fullscreen(bool val){fullscreen = val;}
void GameState::TrapMouse(bool val){trapMouse = val;}
bool GameState::TrapMouse(){return trapMouse;}
bool GameState::InitSDL()
{
  if(SDL_Init(SDL_INIT_EVERYTHING) < 0)
    {
      cerr<<"Error initializing SDL: "<<SDL_GetError()<<endl;
      return false;
    }
  SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
  
  const SDL_VideoInfo *videoInfo;
  videoInfo = SDL_GetVideoInfo( );

  if ( !videoInfo )
    {
      cerr<<"Video query failed: "<<SDL_GetError();
      return false;
    }

  int videoFlags;
  videoFlags  = SDL_OPENGL;          
  videoFlags |= SDL_GL_DOUBLEBUFFER; 
  videoFlags |= SDL_HWPALETTE;       
  videoFlags |= SDL_RESIZABLE;      
  if(fullscreen)
    videoFlags |= SDL_FULLSCREEN;

  if ( videoInfo->hw_available )
    videoFlags |= SDL_HWSURFACE;
  else
    videoFlags |= SDL_SWSURFACE;

  /* This checks if hardware blits can be done */
  if ( videoInfo->blit_hw )
    videoFlags |= SDL_HWACCEL;

  if(!SDL_SetVideoMode( width, height, videoInfo->vfmt->BitsPerPixel, videoFlags ))
    {
      cerr<<"Could not set SDL Video mode: "<<SDL_GetError()<<endl;
      return false;
    }
  SDL_ShowCursor(SDL_DISABLE);
  SDL_WarpMouse(width/2,height/2);
  return true;
}
bool GameState::InitGLContext()
{
  //GL Texture Mapping
  glEnable(GL_TEXTURE_2D);
  glShadeModel(GL_SMOOTH);
  
  //GL Background color
  glClearColor(0,0,1.0f,0);
  glClearDepth(1.0f);
 
  glEnable(GL_LIGHTING);
  GLfloat LightAmbient[] = {1.5f,1.5f,1.5f,1.0f};
  GLfloat LightDiffuse[] = {100.0f,100.0f,100.0f,1.0f};
  GLfloat LightPosition[] = {-100.0f,100.0f,-100.0f,1.0f};

  glLightfv(GL_LIGHT1, GL_AMBIENT, LightAmbient);
  glLightfv(GL_LIGHT1, GL_DIFFUSE, LightDiffuse);
  glLightfv(GL_LIGHT1, GL_POSITION, LightPosition);
  glEnable(GL_LIGHT1);
  
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);

  glEnable(GL_ALPHA_TEST);
  glAlphaFunc(GL_GREATER, 0.1f);
  
  GLfloat fogColor[4] = {0.0f,0.0f,1.0f,1.0f};

  glFogi(GL_FOG_MODE, GL_LINEAR);        // Fog Mode
  glFogfv(GL_FOG_COLOR, fogColor);            // Set Fog Color
  glFogf(GL_FOG_DENSITY, 0.01f);              // How Dense Will The Fog Be
  glHint(GL_FOG_HINT, GL_NICEST);          // Fog Hint Value
  glFogf(GL_FOG_START, 90.0f);             // Fog Start Depth
  glFogf(GL_FOG_END, 100.0f);               // Fog End Depth
  glEnable(GL_FOG);
  
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
  
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  return true;
}
