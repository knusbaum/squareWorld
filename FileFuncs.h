//$Id: FileFuncs.h,v 1.1.1.1 2012/04/08 04:00:33 kyle Exp $
#include <iostream>
#include <stdio.h>
#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>

#ifndef FILEFUNCS_H
#define FILEFUNCS_H

class FileFuncs{
  static SDL_Surface* LoadBMP(char *Filename);
 public:
  static bool LoadGLTexture(GLuint &texture, char *texName, GLfloat GLTexMin, GLfloat GLTexMag);
};

#endif
