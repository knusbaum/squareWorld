#$Id: makefile,v 1.4 2012/04/08 08:27:05 kyle Exp $
CXX = gcc -ggdb -pg
BINNAME = squareWorld
LINKS = -lSDL -lGL -lGLU -lstdc++ -lm
OBJ = 	object/main.o \
	object/GameState.o \
	object/FileFuncs.o \
	object/GameLoop.o \
	object/Square.o \
	object/Position.o \
	object/Trees.o \
	object/DisplayList.o

.PHONY: all clean optimized
all : squareWorld

optimized: CXX = g++ -O3
optimized: squareWorld

pre-build :
	-@mkdir bin 2>>/dev/null || true
	-@mkdir object 2>>/dev/null || true
squareWorld : pre-build $(OBJ)
	$(CXX) $(LINKS) -o bin/$(BINNAME) $(OBJ)

	@rm squareWorld 2>>/dev/null || true
	ln -s bin/$(BINNAME) $(BINNAME)

object/main.o : main.cpp GameState.h FileFuncs.h GameLoop.h Square.h
	$(CXX) -c main.cpp -o object/main.o

object/GameState.o : GameState.cpp GameState.h Square.h
	$(CXX) -c GameState.cpp -o object/GameState.o

object/FileFuncs.o : FileFuncs.cpp FileFuncs.h
	$(CXX) -c FileFuncs.cpp -o object/FileFuncs.o

object/GameLoop.o : GameLoop.cpp GameLoop.h GameState.h Trees.h
	$(CXX) -c GameLoop.cpp -o object/GameLoop.o

object/Square.o : Square.cpp Square.h GameState.h FileFuncs.h Position.h
	$(CXX) -c Square.cpp -o object/Square.o

object/Position.o : Position.cpp Position.h
	$(CXX) -c Position.cpp -o object/Position.o

object/Trees.o : Trees.cpp Trees.h Square.h DisplayList.h
	$(CXX) -c Trees.cpp -o object/Trees.o

object/DisplayList.o : DisplayList.cpp DisplayList.h Square.h
	$(CXX) -c DisplayList.cpp -o object/DisplayList.o
clean :
	@rm bin/* 2>>/dev/null || true
	@rm object/* 2>>/dev/null || true
	@rm squareWorld gmon.out 2>>/dev/null || true

# -@ ignores errors and. 2>>/dev/null directs error output to null, || true makes sure make recieves no errors to report.
