#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "Square.h"

#ifndef DISPLAYLIST_H
#define DISPLAYLIST_H

class DisplayList{

 public:
  static GLuint GenerateList(Square *square);

};

#endif
