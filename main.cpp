//$Id: main.cpp,v 1.9 2012/04/09 00:10:42 kyle Exp $
#include <iostream>
#include "GameState.h"
#include "GameLoop.h"
#include "Square.h"
#include "Trees.h"

#define WIDTH 1024
#define HEIGHT 768


int main(int argc, char** argv)
{
  GameState game = GameState(WIDTH,HEIGHT);
  GameLoop gameLoop = GameLoop(&game);
  
  //game.Fullscreen(true);
  game.TrapMouse(true);
  if(!game.InitSDL())
    return 1;

  if(!game.InitGLContext())
    return 1;

  Trees tree = Trees();

  for(int i = 0; i<100; i++)
    {
      tree.addTree(&game.squares,-120,120,-120,120);
    }
  
  for(int i = 0; i<500; i++)
    {
      tree.addFlower(&game.squares,-50,50,-50,50);
    }
  /**/
  Square *square = new Square();
  //square = new Square();
  square->SetTex((char *)"images/grass2.bmp");
  square->SetSize(1000.0f,1000.0f);
  square->position.RotX(90.0f);
  square->position.MovY(-2.0f);
  square->SetTexRepeat(200.0f, 200.0f);
  square->FakeMotion(true);;
  //game->squares->push_back(*square);
  game.floor = *square;
  delete(square);

  
  game.camPosition.MovY(-1.0f);
  while(game.Running())
    {
      gameLoop.GetInput();
      gameLoop.Update();
      gameLoop.Render();
    }
  SDL_Quit();
  return 0;
}
