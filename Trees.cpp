//$Id: Trees.cpp,v 1.2 2012/04/08 05:54:01 kyle Exp $
#include "Trees.h"

Trees::Trees(){
  
  Square *square = new Square();
  for(int i = 8; i < 13; i++)
    {
      square->SetSize(i/2,i);
      trees[i-7] = DisplayList::GenerateList(square);
    }
  square->SetSize(.4f,.4f);
  trees[0] = DisplayList::GenerateList(square);
  delete(square);
}

void Trees::addTree(vector<Square>* squares, int xmin, int xmax, int zmin, int zmax)
{
  Square *square = makeTree(xmin,xmax,zmin,zmax);
  squares->push_back(*square);
  delete(square);
}

void Trees::addFlower(vector<Square>* squares, int xmin, int xmax, int zmin, int zmax)
{
  Square *square = makeFlower(xmin,xmax,zmin,zmax);
  squares->push_back(*square);
  delete(square);
}

Square* Trees::makeTree(int xmin, int xmax, int zmin, int zmax)
{
  Square *square = new Square();
  square->SetType(1);
  square->FollowCam(true);
  int random = rand()%3;
  switch(random)
    {
    case 0:
      square->SetTex((char*)"images/tree_new.bmp");
      break;
    case 1:
      square->SetTex((char*)"images/tree_new2.bmp");
      break;
    case 2:
      square->SetTex((char*)"images/tree_new3.bmp");
      break;
    }
  int display = (rand()%5)+1;
  square->SetDisplayList(trees[display]);
  float height = (rand()%5)+8;
  square->SetSize(height/2,height);
  //square->position.MovY((height/2)-3);
  square->position.MovY(((trees[display]+7)/2)-3);
  square->position.MovX((rand()%(xmax-xmin))+xmin);
  square->position.MovZ((rand()%(zmax-zmin))+zmin);
  return square;
}

Square* Trees::makeFlower(int xmin, int xmax, int zmin, int zmax)
{
  Square *square = new Square();
  square->SetType(2);
  square->FollowCam(true);
  int random = rand()%3;
  switch(random)
  {
  case 0:
    square->SetTex((char*)"images/flower1.bmp");
    break;
  case 1:
    square->SetTex((char*)"images/flower2.bmp");
    break;
  case 2:
    square->SetTex((char*)"images/flower3.bmp");
    break;
  }
  square->SetSize(.4f, .4f);
  square->position.MovY(-2.8f);
  square->position.MovX((rand()%(xmax-xmin))+xmin);
  square->position.MovZ((rand()%(zmax-zmin))+zmin);
  square->SetDisplayList(trees[0]);
  return square;
}
