//$Id: FileFuncs.cpp,v 1.1.1.1 2012/04/08 04:00:33 kyle Exp $
#include "FileFuncs.h"

SDL_Surface* FileFuncs::LoadBMP(char *Filename)
{
  if(!Filename)
    return NULL;
  
  FILE *bitmap;
  bitmap = fopen(Filename, "r");
  if(bitmap)
    {
      fclose(bitmap);
      return SDL_LoadBMP(Filename);
    }
  return NULL;
}

bool FileFuncs::LoadGLTexture(GLuint &texture, char *texName, GLfloat GLTexMin, GLfloat GLTexMag)
{
  SDL_Surface *bitmap = LoadBMP(texName);
  if(bitmap)
    {
      glGenTextures(1, &texture);
      glBindTexture(GL_TEXTURE_2D, texture);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, bitmap->w, bitmap->h, 0, GL_BGRA, GL_UNSIGNED_BYTE, bitmap->pixels);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GLTexMin);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GLTexMag);
      SDL_FreeSurface(bitmap);
      return true;
    }
  return false;
}
