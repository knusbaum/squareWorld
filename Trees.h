//$Id: Trees.h,v 1.2 2012/04/08 05:54:01 kyle Exp $
#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <vector>
#include "Square.h"
#include "DisplayList.h"

#ifndef TREES_H
#define TREES_H

class Trees{
  GLuint trees[6];
 public:
  Trees();
  void addTree(vector<Square>*,int,int,int,int);
  void addFlower(vector<Square>*,int,int,int,int);
  Square *makeTree(int,int,int,int);
  Square *makeFlower(int,int,int,int);

};

#endif
