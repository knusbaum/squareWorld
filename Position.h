//$Id: Position.h,v 1.1.1.1 2012/04/08 04:00:33 kyle Exp $
#ifndef POSITION_H
#define POSITION_H

class Position{
  float xpos;
  float ypos;
  float zpos;
  float xrot;
  float yrot;
  float zrot;
 public:
  Position();
  float pX();
  float pY();
  float pZ();
  float rX();
  float rY();
  float rZ();
  void SetPos(float,float,float);
  void SetRot(float,float,float);
  void MovX(float);
  void MovY(float);
  void MovZ(float);
  void RotX(float);
  void RotY(float);
  void RotZ(float);
  
  
};

#endif
