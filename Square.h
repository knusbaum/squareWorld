//$Id: Square.h,v 1.2 2012/04/08 05:54:00 kyle Exp $
#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <cmath>
#include "FileFuncs.h"
#include "Position.h"

#ifndef SQUARE_H
#define SQUARE_H

#ifndef PI
#define PI 3.14159265
#endif

using namespace std;

class Square{
  float width;
  float height;
  GLuint texture;
  float texRepeatx;
  float texRepeaty;
  bool followCam;
  bool fakeMotion;
  int type;
  GLuint displayList;
 public:
  Square();
  Position position;
  void SetDisplayList(GLuint);
  void SetTex(GLuint);
  void SetTex(char*);
  void SetTexRepeat(float,float);
  void SetSize(float,float);
  void Render(Position &camPosition);
  void FollowCam(bool);
  void FakeMotion(bool);
  GLuint* Texture();
  void SetType(int);
  int GetType();
  float TexRepeatX();
  float TexRepeatY();
  float Width();
  float Height();  
};

#endif
