//$Id: GameLoop.h,v 1.2 2012/04/08 08:27:05 kyle Exp $
#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <cmath>
#include "GameState.h"
#include "Trees.h"


#ifndef GAMELOOP_H
#define GAMELOOP_H

#ifndef PI
#define PI 3.14159265f
#endif

class GameLoop{
  GameState *game;
  Uint32 lastTime;
  float rotationMultiplier;
 public:
  GameLoop(GameState *);
  void GetInput();
  void Update();
  void Render();
  
};

#endif
