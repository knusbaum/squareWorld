//$Id: GameState.h,v 1.4 2012/04/09 00:10:42 kyle Exp $
#include <iostream>
#include <stdio.h>
#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <vector>
#include <list>
#include "Square.h"
#include "Position.h"


#ifndef GAMESTATE_H
#define GAMESTATE_H

using namespace std;

class GameState{
  bool running;
  bool fullscreen;
  bool trapMouse;
public:
  int width;
  int height;
  Position camPosition;
  float zvelocity;
  float xvelocity;
  float yvelocity;
  float ryvelocity;
  float rxvelocity;
  vector<Square> squares;
  Square floor;
  bool Running();
  void Kill();
  void Fullscreen(bool);
  bool InitSDL();
  bool InitGLContext();
  void TrapMouse(bool);
  bool TrapMouse();
  GameState(int Width,int Height)
    {
      width = Width;
      height = Height;
      running = true;
      squares = vector<Square>();
      camPosition = Position();
      fullscreen = false;
      trapMouse = false;
      zvelocity = 0;
      xvelocity = 0;
      yvelocity = 0;
      ryvelocity = 0;
      rxvelocity = 0;
    }
};

#endif
