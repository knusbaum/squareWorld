#include "DisplayList.h"


GLuint DisplayList::GenerateList(Square *square)
{
  GLuint list = glGenLists(1);
  glNewList(list,GL_COMPILE);
  float texRepeaty = square->TexRepeatY();
  float texRepeatx = square->TexRepeatX();
  float width = square->Width();
  float height = square->Height();
  glBegin(GL_QUADS);
  {
    glNormal3f(-1.0f,0.0f,0.0f);
    glTexCoord2f(0.0f,texRepeaty);       glVertex3f(-width/2,-height/2,0.0f);
    
    glNormal3f(-1.0f,0.0f,1.0f);
    glTexCoord2f(0.0f,0.0f);             glVertex3f(-width/2,height/2,0.0f);
    
    glNormal3f(1.0f,0.0f,1.0f);
    glTexCoord2f(texRepeatx,0.0f);       glVertex3f(width/2,height/2,0.0f);
    
    glNormal3f(1.0f,0.0f,1.0f);
    glTexCoord2f(texRepeatx,texRepeaty); glVertex3f(width/2,-height/2,0.0f);
  }
  glEnd();
  glEndList();
  return list;
}
